#include "task0.hpp"

const int nprocs = 2, N = 1000; // use 2 processes, each performing N operations

int main() {
  queue<int> q;
  { 
    processes ps;
    for (int p = 0; p < nprocs; ++p) {
      ps += [&, p]{ for (int i=0; i<N; ++i) q.dequeue_wait(); };
      ps += [&, p]{ for (int i=0; i<N; ++i) q.enqueue(p); };
    }
  }

  logl("Elements enqueued and dequeued: ", nprocs*N);
  logl("Queue size: ", q.size(), "; elements in the queue: ", count_elements(q));   

  int counts[2] = { 0, 0 };
  { 
    processes ps;
    for (int p = 0; p < nprocs; ++p) {
      ps += [&, p]{ for (int i=0; i<N; ++i) try { q.dequeue(); counts[p]++; } catch (...) {} };
      ps += [&, p]{ for (int i=0; i<N; ++i) q.enqueue(p); };
    }
  }

  logl("Elements enqueued: ", nprocs*N, "; dequeueing attempts: ", nprocs*N, "; successful dequeueings: ", counts[0] + counts[1]);
  logl("Queue size: ", q.size(), "; elements in the queue: ", count_elements(q));   
}
