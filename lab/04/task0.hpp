#include "alang.hpp"

 template <typename E> class queue;    
 template <typename E> class node_iterator;

 // your node implementation
 template <typename E>
 class node { 
   // ...
 };

 template <typename E>
 class node_iterator {
   node<E>* np;
 public:
   node_iterator(node<E>* np) : np(np) {}
   node_iterator& operator++() { assert(!done()); np = np->next; return *this; }
   bool done() const { return np == nullptr; }
   E operator*() const { assert(!done()); return np->data; }
 };

 // your queue implementation
 template <typename E> 
 class queue {
   // ...
 };

 // helper functions
 template <typename E> 
 int count_elements(const queue<E>& q) {
   int ctr = 0; 
   auto it = q.iterator();    
   while (!it.done()) { ++ctr; ++it; }
   return ctr;
 }
