#include "alang.hpp"
#include <iostream>

template<typename E>
class queue;

template<typename E>
class node_iterator;

// your node implementation
template<typename E>
class node {
public:
    E &data;
    node<E> *next;

    node(E &data, node<E> *next) : data(data), next(next) {}
};

template<typename E>
class node_iterator {
    node<E> *np;
public:
    node_iterator(node<E> *np) : np(np) {}

    node_iterator &operator++() {
        assert(!done());
        np = np->next;
        return *this;
    }

    bool done() const { return np == nullptr; }

    E operator*() const {
        assert(!done());
        return np->data;
    }
};


template<typename E>
class queue {
    node<E> *head;
    node<E> *rear;
    int _size;

    semaphore sem_entry = 1; // entry semaphore
    semaphore sem_has_elements = 0; // has

public:
    queue() : head(nullptr), rear(nullptr), _size(0) {}

    void enqueue(E d) {
        node<E> *newNode = new node<E>(d, nullptr);
        sem_entry.P();

        if (empty()) head = newNode; // enqueueing the first element
        else rear->next = newNode;
        rear = newNode;
        _size = _size + 1;

        sem_entry.V();
        sem_has_elements.V();
    }

    E dequeue() {
        if (empty()) throw "empty queue";
        sem_entry.P();

        node<E> *oldHead = head;
        head = head->next;
        if (head == nullptr) rear = nullptr; // removed the last element
        E e = oldHead->data;
        _size = _size - 1;
        delete oldHead;
        sem_entry.V();
        return e;
    }

    E dequeue_wait() {
        sem_has_elements.P();
        return dequeue();
    }

    int size() const { return _size; }

    node_iterator<E> iterator() const { return head; }

    bool empty() const { return _size == 0; }
};


// helper functions
template<typename E>
int count_elements(const queue<E> &q) {
    int ctr = 0;
    auto it = q.iterator();
    while (!it.done()) {
        ++ctr;
        ++it;
    }
    return ctr;
}
