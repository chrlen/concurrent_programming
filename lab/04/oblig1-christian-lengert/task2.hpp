#include "alang.hpp"

template<typename E>
class queue;

template<typename E>
class node_iterator;

// your node implementation
template<typename E>
class node {
public:
    E &data;
    node<E> *next;
    node(E &data, node<E> *next) : data(data), next(next) {}
};

template<typename E>
class node_iterator {
    node<E> *np;
public:
    node_iterator(node<E> *np) : np(np) {}

    node_iterator &operator++() {
        assert(!done());
        np = np->next;
        return *this;
    }

    bool done() const { return np == nullptr; }

    E operator*() const {
        assert(!done());
        return np->data;
    }
};


template<typename E>
class queue : monitor {
    node<E> *head;
    node<E> *rear;
    int _size;
    cond isAccessed_cond;
    cond hasElements_cond;

    bool isAccessed;

public:
    queue() : head(nullptr), rear(nullptr), _size(0), isAccessed(false) {}

    void enqueue(E d) {
        node<E> *newNode = new node<E>(d, nullptr);
        {
            SYNC;
            while(isAccessed) wait(isAccessed_cond);
            isAccessed = true;
            if (empty()) head = newNode; // enqueueing the first element
            else rear->next = newNode;
            rear = newNode;
            _size = _size + 1;
            isAccessed = false;
            signal(isAccessed_cond);
            signal(hasElements_cond);
        }
    }

    E dequeue() {
        SYNC;
        if (empty()) throw "empty queue";

        while(isAccessed){ wait(isAccessed_cond); }

        node<E> *oldHead = head;
        isAccessed = true;
        head = head->next;
        if (head == nullptr) rear = nullptr; // removed the last element
        E e = oldHead->data;
        delete oldHead;
        _size = _size - 1;
        isAccessed = false;
        signal(isAccessed_cond);
        return e;
    }

    E dequeue_wait() {
        while(self.empty()) wait(hasElements);
        return this->dequeue();
    }

    int size() const { return _size; }

    node_iterator<E> iterator() const { return head; }

    bool empty() const { return _size == 0; }
};


// helper functions
template<typename E>
int count_elements(const queue<E> &q) {
    int ctr = 0;
    auto it = q.iterator();
    while (!it.done()) {
        ++ctr;
        ++it;
    }
    return ctr;
}
