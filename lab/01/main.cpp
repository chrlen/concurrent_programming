#include "alang.hpp"

#include <vector>
#include <cassert>
#include <cmath>
#include <iostream>

template<typename T>
class matrix {
    int r, c;
    std::vector<T> v;
public:
    matrix(int rows, int cols) : r(rows), c(cols) { v.reserve(rows * cols); }

    T &operator()(int i, int j) {
        //assert(validIndex(i, j)); // comment this out before timing tests
        return v[i * r + j];
    }

    const T &operator()(int i, int j) const { return v[i * r + j]; }

    int rows() const { return r; }

    int cols() const { return c; }

    bool validIndex(int i, int j) const { return i >= 0 && i < rows() && j >= 0 && j < cols(); }
};

template<typename T>
matrix<T> operator+(const matrix<T> &a, const matrix<T> &b) {
    assert(a.rows() == b.rows() && a.cols() == b.cols()); // matrix shape mismatch
    matrix<T> sum(a.rows(), a.cols());
    for (int i = 0; i < a.rows(); ++i) {
        for (int j = 0; j < a.cols(); ++j) {
            sum(i, j) = a(i, j) + b(i, j);
        }
    }
    return sum;
}

template<typename T>
matrix<T> constant_matrix(int rows, int cols, T value) {
    matrix<double> m(rows, cols);
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
            m(i, j) = value;
    return m;
}

template<typename T>
matrix<T> row_add(const matrix<T> &a, const matrix<T> &b) {
    processes ps;
    matrix<T> sum(a.rows(), a.cols());
    for (int i = 0; i < a.rows(); ++i) {
        ps += [&, i]{
            for (int j = 0; j < a.cols(); ++j) {
                sum(i, j) = a(i, j) + b(i, j);
            }
        };
    }
    return sum;
}

template<typename T>
matrix<T> col_add(const matrix<T> &a, const matrix<T> &b) {
    processes ps;
    matrix<T> sum(a.rows(), a.cols());
    for (int j = 0; j < a.cols(); ++j) {
        ps += [&, j]{
            for (int i = 0; i < a.rows(); ++i) {
                sum(i, j) = a(i, j) + b(i, j);
            }
        };
    }
    return sum;
}


template<typename T>
matrix<T> elem_add(const matrix<T> &a, const matrix<T> &b) {
    processes ps;
    matrix<T> sum(a.rows(), a.cols());
    for (int i = 0; i < a.rows(); ++i) {
        for (int j = 0; j < a.cols(); ++j) {
            ps += [&, i, j]{
                sum(i, j) = a(i, j) + b(i, j);
            };
        }
    }
    return sum;
}

int main() {
    int matsize = 10000;
    auto m1 = constant_matrix(matsize, matsize, 1.0);
    auto m2 = constant_matrix(matsize, matsize, 3.14);
    auto m3 = matrix<double>(matsize, matsize);

    // time taken to add two million-element matrices
    auto t_serial = time_ms([&] {
        m3 = m1 + m2;
    });
    std::cout << "TIME SER: " << t_serial << std::endl;

    auto t_col = time_ms([&] {
        m3 = col_add(m1,m2);
    });

    std::cout << "TIME COL: " << t_col << std::endl;

    auto t_row = time_ms([&] {
        m3 = row_add(m1,m2);
    });
    std::cout << "TIME ROW: " << t_row << std::endl;

    auto t_elem = time_ms([&] {
        m3 = elem_add(m1,m2);
    });
    std::cout << "TIME ELE: " << t_elem << std::endl;

    // a test that the addition does something sensible
    double epsilon = 0.000001;
    assert(std::abs(m3(140, 323) - 4.14) < epsilon);
}