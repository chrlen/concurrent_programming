cmake_minimum_required(VERSION 3.14)
project(01)
find_package (Threads)
set(CMAKE_CXX_STANDARD 17)

if (CMAKE_BUILD_TYPE EQUAL "DEBUG")

    message("debug mode")
endif (CMAKE_BUILD_TYPE EQUAL "DEBUG")

if (CMAKE_BUILD_TYPE EQUAL "RELEASE")
    set( CMAKE_CXX_FLAGS "-Wall -m64 -O0 " )
    message("release mode")
endif (CMAKE_BUILD_TYPE EQUAL "DEBUG")

add_executable(01 main.cpp)
target_link_libraries (01 ${CMAKE_THREAD_LIBS_INIT})