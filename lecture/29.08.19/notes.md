
< await (B) S>

# How is atomicity implemented?
- Mutual exclusion is needed
- Make shure only atomic code runs
- Use some kind of transactional protocol
# Transactional Memory


Log transactions 
Transaction begin               Trans B
- x = x -1                      x = x -1
- if(x == 0) launch_rockts()    if(x == 0) launch_rockets();
- op3
Transaction end

- Optimistic: No locks, just try

# Implement locks
- access to critical sections
- at most one CS should be running at any time
## Mutual Exclusion
process p [i = 1 to N]{
    while(true){
        CS Entry
        Critical Section(CS)
        CSexit
    }
}
Use lock
process p [i = 1 to N]{
    while(true){
        syncronized(lock)
        Critical Section(CS)
    }
}

### Example

int m = 1;
process p1{
    while(true){
        while(m == 2){}
        cs
        in =2;
        non-cs
    }
}


process p2{
    while(true){
        while(m == 1){}
        cs
        in = 1;
        non-cs
    }
}
- busy wait
- hard time assignment
- does not react to skewness of workload

#### Desired Properties
 - mutual exclusion
 - no deadlock 
 - abscence of unnecessary delay
   - if no other process is executing a CS, a process can enter (Violated here)
 - eventual entry
   - a process waiting, to enter CS will eventually succeed   
    

```
bool lock = false;
process [i = 1 to N]{
while(true){
        <await (!lock); lock = true>
        CS
        lock = false;
        non-CS
    }
}
```
#### Satisfies
- mutex
- abscence of UNW
- no deadlock
- eventual entry

Atomics are implemented in hardware
CS in software

### Test And Set
Semantics
```
TS(lock){
    <bool initial = lock;
    lock = true;
    return initial;
    >
}
```
If another process locked it stays true and loops
Else lock is aquired
```
while(TS(lock));
```
lock is written all the time

### Test and Test and Set

```
bool lock = false;
process p[i=1 to N]{
    while(true){
        while(lock);
        while(TS(lock));

    }
}
```

## Implement Await
(in terms of cs entry/exit protocols)
```
<S> expands to:
CS entry
S
CS Exit

<await(B); S> expands to:
CS Entry
while(!B){
    CS Exit
    DELAY
    CS Entry
}
S
CS Exit

```


## Eventual Entry / Scheduling
### Enabledness
A process is enabled if it can in principle execute next

### Scheduling
Pick one of teh enabled processes for execution
### Fair Scheduling
Informally : Do not neglect an enabled process completely
#### Example
Outcome depends on Scheduling algorithm

```
bool lock = true;
co while(x); || x = false; oc
```
### Unconditional Fairness
each unconditional atomic action is eventually chosen
### Weakly Fair Scheduler
- unconditionally fair 
- every conditional atomic action will eventually be chosen assuming its condition **becomes true** and **stays true**
### Example
If we have a weakly fair scheduler the condition will come true
The await statement will at some point be executed

```
bool x = true;
int y = 0;
co while (x) y = y + 1; || <await(y >= 10);>  x= false oc
```
In practice: Schedulers are weakly fair!


### Strongly Fair Scheduler
- unconditionally fair 
- each conditional atomic action will eventually be chosen for execution if it's condition becomes true **infinitly often**.


```
bool x = true;
co 
    while(x) {y = true; y = false;} ||
    <await(y) x = false>
oc
```